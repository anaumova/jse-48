package ru.tsc.anaumova.tm.api.service.model;

import ru.tsc.anaumova.tm.api.repository.model.IRepository;
import ru.tsc.anaumova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}